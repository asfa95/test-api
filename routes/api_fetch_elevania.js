var m_product = require(__dirname+'/../model/m_product')
var m_product_image = require(__dirname+'/../model/m_product_image')
var elevania= require(__dirname+'/../utils/tools_elevania')
const Joi = require('joi');

async function getDetailProduct(product_id,prd_no){
	let url = `/product/details/${prd_no}`
	let productDetail = await elevania.request({},url)
	console.log(productDetail)
	if(productDetail.Product){
		var jP = productDetail.Product
		
		//Update Product Description
		var data = {
			id:product_id,
			description : jP.htmlDetail.toString()
		}
		let updateProduct = await m_product.updateProduct(data)

		//Insert Image
		let oneTime = true
		for(let key in jP) {
			if(key.includes('prdImage')){
				var dataImage = {
					product_id:product_id,
					file:jP[key][0]
				}
				if(oneTime){
					oneTime=false
					m_product_image.deleteProductImage(dataImage)
				}
				m_product_image.addProductImage(dataImage)
			}
		}

	}
}

async function insertToDB (data){
	let insertedData = await m_product.addProduct(data)
	getDetailProduct(insertedData.id,data.prd_no)
}

var api = {
	get:{
		method: 'GET',
		path: '/fetch/elevania',
		options: {
	        validate: {
	            query: Joi.object({
	                page: Joi.string().min(1).max(2).required()
	            })
	        }
	    },
		handler: async (request, h) => {
			let param=request.query
			let url = `/product/listing?page=${param.page}`
			let json = await elevania.request(param,url)
			console.log(json)
			if(json.Products){
				json.Products.product.forEach((row)=>{
					const data = {
						name:row.prdNm[0],
						sku:'ELV-'+row.dispCtgrNo[0]+'-'+row.prdNo[0],
						prd_no:row.prdNo[0],
						price:row.selPrc[0]
					}
					insertToDB(data)

				})
				console.log('afxxx')
			}
			return json;
		}
	}
}
module.exports = api