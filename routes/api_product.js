var m_product = require(__dirname+'/../model/m_product')
const Joi = require('joi');

var api = {
	get:{
		method: 'GET',
		path: '/product',
		options: {
	        validate: {
	            query: Joi.object({
	                page: Joi.string().required()
	            })
	        }
	    },
		handler:  (request, h) => {
			let param=request.query
			let response = m_product.getProduct(param)
			return response;
		},

	},
	post:{
	    method: 'POST',
	    path: '/hello/{name}',
	    handler: function (request, h) {

	        return `Hello ${request.params.name}!`;
	    },
	    options: {
	        validate: {
	            params: Joi.object({
	                name: Joi.string().min(3).max(10)
	            })
	        }
	    }
	},
	delete:{
	    method: 'DELETE',
	    path: '/product/{id}',
	    handler: function (request, h) {
	        let param=request.params
			let response = m_product.deleteProduct(param)
			return {success:1}
	    },
	    options: {
	        validate: {
	            params: Joi.object({
	                id: Joi.string().required()
	            })
	        }
	    }
	}
}
module.exports = api