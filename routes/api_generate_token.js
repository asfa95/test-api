const tools_security = require(__dirname+'/../utils/tools_security')
const Joi = require('joi');

var api = {
	get:{
		method: 'GET',
		path: '/generate/token',
		handler:  (request, h) => {
			var expiredInHours = 24
			var data = {
				id_user:'123456'
			}
			// var token = tools_security.encrypt_jwt_periode(expiredInHours,data)
			var token = tools_security.encrypt_jwt(data)
			return {
				"token":token
			};
		},

	}
}
module.exports = api