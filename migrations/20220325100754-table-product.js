'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  var sql = `
  CREATE TABLE public.product
  (
      id bigserial,
      name character varying(255),
      prd_no character varying(255),
      sku character varying(255),
      price double precision,
      description text,
      status integer,
      time_create time without time zone,
      time_update time without time zone,
      PRIMARY KEY (id)
  );

  ALTER TABLE public.product
      OWNER to postgres;
          `
  return db.runSql(sql, null, callback);
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
