'use strict';

const Hapi = require('@hapi/hapi');
const middleware = require(__dirname+'/middleware/auth')
const api_product = require(__dirname+'/routes/api_product')
const api_generate_token = require(__dirname+'/routes/api_generate_token')
const api_fetch_elevania = require(__dirname+'/routes/api_fetch_elevania')
const Joi = require('joi');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost',
        "routes": {
            "cors": {
                "origin": ["*"],
                "headers": ["Accept", "Content-Type","Token","token","content-type","Access-Control-Allow-Origin","access-control-allow-origin"],
                "additionalHeaders": ["X-Requested-With","Access-Control-Allow-Origin","token"]
            }
        }
    });

    server.ext('onRequest', function (request, h) {
        if(middleware.validationToken(request)){
            return h.continue;
        }else{
            request.setUrl('/');
            return h.continue;
        }
    });

    server.route({
        method: ['GET','DELETE','POST','PUT'],
        path: '/',
        handler: (request, h) => {
            return {
                message:"Not Authorized"
            }
        }
    });

    server.route(api_product.get)
    server.route(api_product.post)
    server.route(api_product.delete)
    server.route(api_generate_token.get)
    server.route(api_fetch_elevania.get)


    await server.start();
    console.log('Server running on %s', server.info.uri);

    return server
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();

