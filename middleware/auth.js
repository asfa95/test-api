const tools_security = require(__dirname+'/../utils/tools_security')
module.exports = this;
this.validationToken = (request) =>{
	if(request.url.pathname=='/generate/token'){
		return true;
	}

	var token = request.headers['token']
	if(token){
		var valid = tools_security.decrypt_jwt(token)
		if(valid){
			return true
		}else{
			return false
		}
	}else{
		return false
	}
}