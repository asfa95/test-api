var tools_db = require(__dirname+'/../utils/tools_db')
module.exports = this;

this.getProduct = (param)=>{
	return new Promise((resolve, reject) => {
		var offset = 0
		var perpage = 10
		if(param.page==1){
			offset = 0
		}else{
			offset = (param.page - 1) * perpage
		}
		tools_db.manyOrNone("SELECT p.*, "+
     	" json_agg(json_build_object('image_id', pi.id "+
		" ,'product_id', pi.product_id "+
        " ,'image', pi.file "+
        " )) AS images "+
		"FROM   product p "+
		"left JOIN   product_image pi on pi.product_id = p.id where p.status = 1 "+
		"GROUP  BY p.id limit "+perpage+" offset "+offset,param,(data)=>{
			return resolve(data)
		},function(err){
			return reject(err)
		},{});
	})
}

this.addProduct = (param)=>{
	return new Promise((resolve, reject) => {
		tools_db.one('INSERT INTO product( name, prd_no, sku, price, status, time_create) VALUES (${name}, ${prd_no}, ${sku},${price}, 1, now()) ON CONFLICT DO NOTHING RETURNING id;',param,(data)=>{
			return resolve(data)
		},function(err){
			// return reject(err)
		},{});
	})
}

this.getOneProduct = (param)=>{
	return new Promise((resolve, reject) => {
		tools_db.many('SELECT * FROM product where id = ${id}',param,(data)=>{
			return resolve(data)
		},function(err){
			return reject(err)
		},{});
	})
}

this.updateProduct = (param)=>{
	return new Promise((resolve, reject) => {
		tools_db.any('UPDATE public.product '+
		' SET description=${description}, time_update=now() '+
		' WHERE id=${id} returning id ',param,(data)=>{
			return resolve(data)
		},function(err){
			return reject(err)
		},{});
	})
}

this.deleteProduct = (param)=>{
	return new Promise((resolve, reject) => {
		tools_db.none('UPDATE public.product '+
		' SET status=2, time_update=now() '+
		' WHERE id=${id}',param,(data)=>{
			return resolve(data)
		},function(err){
			return reject(err)
		},{});
	})
}
