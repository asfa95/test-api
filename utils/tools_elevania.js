var request = require('request');
var xml2js= require(__dirname+'/tools_xml2js')
require('dotenv').config();

module.exports = this;

this.request = (param,url)=>{
  return new Promise((resolve, reject) => {
    var options = {
      'method': 'GET',
      'url': 'http://api.elevenia.co.id/rest/prodservices'+url,
      'headers': {
        'openapikey': process.env.ELEVANIA_KEY,
      }
    };
    console.log('PARAMS:',options)
    request(options, function (error, response) {
      if (error) throw new Error(error);
      console.log('RESPONSE:',response.body);
      let jsonData = xml2js.toJson(response.body)
      return resolve(jsonData)
    });

  })
}
