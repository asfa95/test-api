module.exports = this;
require('dotenv').config();

var pgp = require("pg-promise")(/*options*/);
this.pgp = pgp;
// console.log(JSON.stringify(config));
var db = pgp("postgres://"+process.env.DB_USER+":"+process.env.DB_PASS+"@"+process.env.DB_HOST+":"+process.env.DB_PORT+"/"+process.env.DB_NAME);
var types = pgp.pg.types;

function pgToString(value) {
    return value.toString();
}

types.setTypeParser(1082, pgToString); // date
types.setTypeParser(1083, pgToString); // time
types.setTypeParser(1114, pgToString); // timestamp
types.setTypeParser(1184, pgToString); // timestamptz
types.setTypeParser(1266, pgToString); // timetz

// types.setTypeParser(1114, function(stringValue) {
// 	return stringValue;
// });

this.one = function(script,parameter,callback_ok,callback_err,req){
	let tag = '-'
	if(typeof req !== 'undefined') tag= req.TAG; 
	db.one(script,parameter).then(function(data){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:OK;"+script+";"+JSON.stringify(parameter));
		callback_ok(data);
	}).catch(function(err){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:FAIL;"+script+";"+JSON.stringify(parameter)+";"+JSON.stringify(err));
		callback_err(err);
	});
};

this.many = function(script,parameter,callback_ok,callback_err,req){
	let tag = '-'
	if(typeof req !== 'undefined') tag= req.TAG; 
	db.many(script,parameter).then(function(data){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:OK;"+script+";"+JSON.stringify(parameter));
		callback_ok(data);
	}).catch(function(err){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:FAIL;"+script+";"+JSON.stringify(parameter)+";"+JSON.stringify(err));
		callback_err(err);
	});
};

this.any = function(script,parameter,callback_ok,callback_err,req){
	let tag = '-'
	if(typeof req !== 'undefined') tag= req.TAG; 
	db.any(script,parameter).then(function(data){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:OK;"+script+";"+JSON.stringify(parameter));
		callback_ok(data);
	}).catch(function(err){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:FAIL;"+script+";"+JSON.stringify(parameter)+";"+JSON.stringify(err));
		callback_err(err);
	});
};

this.manyOrNone = function(script,parameter,callback_ok,callback_err,req){
	let tag = '-'
	if(typeof req !== 'undefined') tag= req.TAG; 
	db.manyOrNone(script,parameter).then(function(data){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:OK;"+script+";"+JSON.stringify(parameter));
		callback_ok(data);
	}).catch(function(err){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:FAIL;"+script+";"+JSON.stringify(parameter)+";"+JSON.stringify(err));
		callback_err(err);
	});
};
this.oneOrNone = function(script,parameter,callback_ok,callback_err,req){
	let tag = '-'
	if(typeof req !== 'undefined') tag= req.TAG; 
	db.oneOrNone(script,parameter).then(function(data){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:OK;"+script+";"+JSON.stringify(parameter));
		callback_ok(data);
	}).catch(function(err){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:FAIL;"+script+";"+JSON.stringify(parameter)+";"+JSON.stringify(err));
		callback_err(err);
	});
};
this.none=function(script,parameter,callback_ok,callback_err,req){
	let tag = '-'
	if(typeof req !== 'undefined') tag= req.TAG; 
	db.none(script,parameter).then(function(){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:OK;"+script+";"+JSON.stringify(parameter));
		callback_ok();
	}).catch(function(err){
		if(typeof req === 'undefined' || typeof req.writeLogs === 'undefined' || req.writeLogs) console.log(tag,"DB:FAIL;"+script+";"+JSON.stringify(parameter)+";"+JSON.stringify(err));
		callback_err(err);
	});
};
