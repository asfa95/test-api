# Install
```
npm install
npm install db-migrate --save
npm install db-migrate-pg —save
npm install dotenv --save
npm install jsonwebtoken --save
npm install request --save
npm install xml2js --save
```

# DB
```
CREATE DATABASE hapi
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1; 
```

# Migrasi Database
```
db-migrate up
```

# Jalankan
```
node server.js
```

# Download data ke database
- Import postman link berikut:
https://www.getpostman.com/collections/a63c7d34ed0e264c5246
- Kirim request pada postman :
    - Nama Request: *Fetch Elevania To DB*
    - isi parameter *page* - 1 sampai 3
    - data akan terdownload ke DB

